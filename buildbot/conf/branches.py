# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2011-2024 Blender Authors
# <pep8 compliant>

import copy

from collections import OrderedDict

# Blender repository branches used for daily builds and API doc generation.
code_tracked_branch_ids = {
    "vdev": "main",
    "vexp": "",
    "v330": "blender-v3.3-release",
    "v360": "blender-v3.6-release",
    "v410": "blender-v4.1-release",
}

# Processor architectures to build for each track.
code_official_platform_architectures = {
    "vdev": ["darwin-x86_64", "darwin-arm64", "linux-x86_64", "windows-amd64"],
    "vexp": ["darwin-x86_64", "darwin-arm64", "linux-x86_64", "windows-amd64"],
    "v330": ["darwin-x86_64", "darwin-arm64", "linux-x86_64", "windows-amd64"],
    "v360": ["darwin-x86_64", "darwin-arm64", "linux-x86_64", "windows-amd64"],
    "v410": ["darwin-x86_64", "darwin-arm64", "linux-x86_64", "windows-amd64"],
}

# Windows ARM64 not used by default yet.
code_all_platform_architectures = copy.deepcopy(code_official_platform_architectures)
code_all_platform_architectures["vdev"].append("windows-arm64")
code_all_platform_architectures["vexp"].append("windows-arm64")

track_major_minor_versions = {
    "vdev": "4.2",
    "vexp": "4.2",
    "v410": "4.1",
    "v360": "3.6",
    "v330": "3.3",
}

# Blender code and manual git branches.
track_code_branches = {
    "vdev": "main",
    "vexp": "main",
    "v410": "blender-v4.1-release",
    "v360": "blender-v3.6-release",
    "v330": "blender-v3.3-release",
}

# Tracks that correspond to an LTS version released on the Windows Store.
# Only add entries here AFTER the regular release is out, since it will
# otherwise generate the wrong package for the regular release.
windows_store_lts_tracks = ["v330", "v360"]

# Tracks that correspond to active and upcoming LTS releases. Used for
# the Snap track name, and for Steam to determine if there is a daily LTS
# track to upload to.
all_lts_tracks = ["v330", "v360"]

# Tracks for automated delivery of daily builds to stores.
code_store_track_ids = [
    "vdev",
    "v330",
    "v360",
    "v410",
]

# Tracks to deploy releases (regular and LTS) to download.blender.org.
code_deploy_track_ids = {
    "v330": None,
    "v360": None,
    "v410": None,
}

# Stable track for manual and API docs.
# Update on release.
doc_stable_major_minor_version = "4.1"

# Versions and labels for the user manual version switching menu.
# Update when creating new release branch, and on release.
doc_manual_version_labels = OrderedDict(
    [
        ("2.79", "2.79"),
        ("2.80", "2.80"),
        ("2.81", "2.81"),
        ("2.82", "2.82"),
        ("2.83", "2.83 (LTS)"),
        ("2.90", "2.90"),
        ("2.91", "2.91"),
        ("2.92", "2.92"),
        ("2.93", "2.93 (LTS)"),
        ("3.0", "3.0"),
        ("3.1", "3.1"),
        ("3.2", "3.2"),
        ("3.3", "3.3 (LTS)"),
        ("3.4", "3.4"),
        ("3.5", "3.5"),
        ("3.6", "3.6 (LTS)"),
        ("4.0", "4.0"),
        ("4.1", "4.1"),
        ("4.2", "4.2 (develop)"),
    ]
)
