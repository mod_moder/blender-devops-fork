# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2011-2024 Blender Authors
# <pep8 compliant>

_worker_names = {
    "code-lint": ["localhost"],
    "linux-x86_64-code": ["localhost"],
    "linux-x86_64-code-gpu": ["localhost"],
    "linux-x86_64-doc-api": ["localhost"],
    "linux-x86_64-doc-studio-pipeline": ["localhost"],
    "linux-x86_64-general": ["localhost"],
    "linux-x86_64-store-snap": ["localhost"],
    "linux-x86_64-store-steam": ["localhost"],
    "darwin-arm64-code": ["localhost"],
    "darwin-arm64-code-gpu": ["localhost"],
    "darwin-x86_64-code": ["localhost"],
    "darwin-x86_64-code-gpu": ["localhost"],
    "windows-amd64-code": ["localhost"],
    "windows-amd64-code-gpu": [],
    "windows-amd64-store-windows": ["localhost"],
    "windows-arm64-code": ["localhost"],
    "windows-arm64-code-gpu": [],
}


def get_worker_password(worker_name: str) -> str:
    return "localhost"


def get_worker_names(devops_env_id: str):
    return _worker_names
