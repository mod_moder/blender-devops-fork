# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2011-2024 Blender Authors
# <pep8 compliant>

import importlib

from buildbot.plugins import changes as plugins_changes

import conf.branches

import pipeline.common
import pipeline.code
import pipeline.code_benchmark
import pipeline.code_deploy
import pipeline.code_bpy_deploy
import pipeline.code_store
import pipeline.doc_api
import pipeline.doc_manual
import pipeline.doc_developer
import pipeline.doc_studio

importlib.reload(pipeline.common)
importlib.reload(conf.branches)


def populate(devops_env_id):
    pipelines_modules = [
        pipeline.code,
        pipeline.code_benchmark,
        pipeline.code_deploy,
        pipeline.code_bpy_deploy,
        pipeline.code_store,
        pipeline.doc_api,
        pipeline.doc_manual,
        pipeline.doc_developer,
        pipeline.doc_studio,
    ]

    builders = []
    schedulers = []

    for pipelines_module in pipelines_modules:
        importlib.reload(pipelines_module)
        b, s = pipelines_module.populate(devops_env_id)
        builders += b
        schedulers += s

    return builders, schedulers


def change_sources():
    branch_ids = list(conf.branches.code_tracked_branch_ids.values())

    pollers = []
    poll_interval_in_seconds = 2 * 60

    pollers += [
        plugins_changes.GitPoller(
            repourl="https://projects.blender.org/blender/blender.git",
            pollAtLaunch=True,
            pollinterval=poll_interval_in_seconds,
            workdir="blender-gitpoller-workdir",
            project="blender.git",
            branches=branch_ids,
        )
    ]

    pollers += [
        plugins_changes.GitPoller(
            repourl="https://projects.blender.org/blender/blender-manual.git",
            pollAtLaunch=True,
            pollinterval=poll_interval_in_seconds,
            workdir="blender-manual-gitpoller-workdir",
            project="blender-manual.git",
            branches=branch_ids,
        )
    ]

    pollers += [
        plugins_changes.GitPoller(
            repourl="https://projects.blender.org/blender/blender-developer-docs.git",
            pollAtLaunch=True,
            pollinterval=poll_interval_in_seconds,
            workdir="blender-developer-docs-gitpoller-workdir",
            project="blender-developer-docs.git",
            branches=["main"],
        )
    ]

    pollers += [
        plugins_changes.GitPoller(
            repourl="https://projects.blender.org/studio/blender-studio-pipeline.git",
            pollAtLaunch=True,
            pollinterval=poll_interval_in_seconds,
            workdir="blender-studio-pipeline-gitpoller-workdir",
            project="blender-studio-pipeline.git",
            branches=["main"],
        )
    ]

    return pollers
