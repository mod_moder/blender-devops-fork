# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2011-2024 Blender Authors
# <pep8 compliant>

import pathlib
from functools import partial

import buildbot.plugins
from buildbot.plugins import steps as plugins_steps

import conf.branches
import conf.worker
import pipeline.common


# Custom file upload that shows links to download files.
class LinkMultipleFileUpload(plugins_steps.MultipleFileUpload):
    def uploadDone(self, result, source, masterdest):
        if not self.url:
            return

        name = pathlib.Path(source).name
        self.addURL(name, self.url + "/" + name + "/report.html")

    def allUploadsDone(self, result, sources, masterdest):
        return


def create_deliver_step(devops_env_id):
    worker_config = conf.worker.get_config(devops_env_id)

    file_size_in_mb = 500 * 1024 * 1024
    worker_source_path = pathlib.Path(f"../../../../git/blender-vdev/build_package")
    master_dest_path = worker_config.buildbot_download_folder / "daily" / "benchmarks"

    return LinkMultipleFileUpload(
        name="deliver",
        maxsize=file_size_in_mb,
        workdir=f"{worker_source_path}",
        glob=True,
        workersrcs=["main-*"],
        masterdest=f"{master_dest_path}",
        mode=0o644,
        url=f"../download/daily/benchmarks",
        description="running",
        descriptionDone="completed",
        alwaysRun=True,
    )


def populate(devops_env_id):
    properties = [
        buildbot.plugins.util.StringParameter(
            name="commit_id",
            label="Commit:",
            required=True,
            size=80,
            default="HEAD",
        ),
        buildbot.plugins.util.BooleanParameter(
            name="needs_gpu_binaries",
            label="GPU binaries -> build Cycles GPU kernels",
            required=True,
            strict=True,
            default=True,
            hide=True,
        ),
    ]

    return pipeline.common.create_pipeline(
        devops_env_id,
        "code-benchmark",
        "code_benchmark.py",
        [
            "configure-machine",
            "update-code",
            "compile-code",
            "compile-gpu",
            "compile-install",
            "benchmark",
            partial(create_deliver_step, devops_env_id),
            "clean",
        ],
        {"vdev": "main"},
        properties,
        "blender.git",
        ["linux-x86_64-code-gpu", "darwin-arm64-code-gpu"],
        # Compile GPU step needs a long timeout.
        default_step_timeout_in_seconds=90 * 60,
        variations=["linux", "darwin"],
        nightly_properties={"commit_id": "HEAD", "needs_gpu_binaries": True},
        hour=7,
        minute=30,
    )
