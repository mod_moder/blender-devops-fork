# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2011-2024 Blender Authors
# <pep8 compliant>

import os
import shutil
import sys

from typing import List

import worker.blender
import worker.blender.pack
import worker.blender.compile


def get_ctest_arguments(builder: worker.blender.CodeBuilder) -> List[str]:
    args = ["--output-on-failure"]
    args += ["--timeout", "400"]

    # Run tests in single threads for tracks which has older OpenEXR/OpenImageIO,
    # This prevents idiff.exe dead-lock on exit.
    if not (builder.platform == "windows" and builder.track_id == "v330"):
        # GPU tests are currently slow and can cause timeouts.
        if not builder.needs_gpu_tests:
            args += ["--parallel", "4"]

    args += ["-C", worker.blender.compile.get_cmake_build_type(builder)]
    return args


def package_for_upload(builder: worker.blender.CodeBuilder, success: bool) -> None:
    build_tests_dir = builder.build_dir / "tests"
    package_tests_dir = builder.package_dir / "tests"
    if not build_tests_dir.exists():
        return

    os.makedirs(package_tests_dir, exist_ok=True)

    # Upload package on failure
    if not success:
        package_filename = "tests-" + worker.blender.pack.get_package_name(builder)
        package_filepath = package_tests_dir / package_filename
        shutil.copytree(build_tests_dir, package_filepath)
        shutil.make_archive(str(package_filepath), "zip", package_tests_dir, package_filename)
        shutil.rmtree(package_filepath)

    # Always upload unpacked folder for main and release tracks,
    # when using GPU tests. This is useful for debugging GPU
    # differences.
    if builder.track_id != "vexp" and builder.needs_gpu_tests:
        branch = builder.branch_id.replace("blender-", "").replace("-release", "")
        name = f"{branch}-{builder.platform}-{builder.architecture}"
        shutil.copytree(build_tests_dir, package_tests_dir / name)


def test(builder: worker.blender.CodeBuilder) -> None:
    builder.setup_build_environment()
    os.chdir(builder.build_dir)
    success = False

    try:
        builder.call(["ctest"] + get_ctest_arguments(builder))
        success = True
    finally:
        package_for_upload(builder, success)
