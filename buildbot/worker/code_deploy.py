#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2011-2024 Blender Authors
# <pep8 compliant>

import pathlib
import sys

from collections import OrderedDict
from typing import Callable

sys.path.append(str(pathlib.Path(__file__).resolve().parent.parent))

import worker.configure
import worker.utils

import worker.blender
import worker.blender.update

import worker.deploy
import worker.deploy.source
import worker.deploy.artifacts
import worker.deploy.monitor


if __name__ == "__main__":
    steps: worker.utils.BuilderSteps = OrderedDict()
    steps["configure-machine"] = worker.configure.configure_machine
    steps["update-code"] = worker.blender.update.update
    steps["pull-artifacts"] = worker.deploy.artifacts.pull
    steps["repackage-artifacts"] = worker.deploy.artifacts.repackage
    steps["package-source"] = worker.deploy.source.package
    steps["deploy-artifacts"] = worker.deploy.artifacts.deploy
    steps["monitor-artifacts"] = worker.deploy.monitor.monitor
    steps["clean"] = worker.deploy.CodeDeployBuilder.clean

    parser = worker.blender.create_argument_parser(steps=steps)

    args = parser.parse_args()
    builder = worker.deploy.CodeDeployBuilder(args)
    builder.run(args.step, steps)
