#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2011-2024 Blender Authors
# <pep8 compliant>

import argparse
import os
import pathlib
import sys

from collections import OrderedDict

sys.path.append(str(pathlib.Path(__file__).resolve().parent.parent))

import worker.utils


class DocStudioBuilder(worker.utils.Builder):
    def __init__(self, args: argparse.Namespace):
        super().__init__(args, "studio", "blender-studio-pipeline")
        self.setup_track_path()


def update(builder: worker.utils.Builder) -> None:
    builder.update_source(update_submodules=True)


def compile_doc(builder: worker.utils.Builder) -> None:
    docs_path = builder.code_path / "docs"
    os.chdir(docs_path)

    worker.utils.call(["npm", "install"])
    worker.utils.call(["npm", "run", "docs:build"])


def deliver(builder: worker.utils.Builder) -> None:
    dry_run = False
    if builder.service_env_id not in ("PROD", "LOCAL"):
        worker.utils.warning("Delivery from non-PROD is dry run only")
        dry_run = True

    worker_config = builder.get_worker_config()
    connect_id = f"{worker_config.studio_user}@{worker_config.studio_machine}"
    change_modes = ["D0755", "F0644"]

    if builder.service_env_id == "LOCAL" and builder.platform == "darwin":
        worker.utils.warning("rsync change_owner not supported on darwin, ignoring for LOCAL")
        change_owner = None
    else:
        change_owner = "buildbot:www-data"

    # Content of the website.
    docs_local_path = builder.code_path / "docs" / ".vitepress" / "dist"
    docs_remote_path = pathlib.Path(worker_config.studio_folder)

    docs_source_path = f"{docs_local_path}/"
    docs_dest_path = f"{connect_id}:{docs_remote_path}/"
    worker.utils.rsync(
        docs_source_path,
        docs_dest_path,
        change_modes=change_modes,
        change_owner=change_owner,
        port=worker_config.studio_port,
        dry_run=dry_run,
    )

    # Downloadable artifacts.
    artifacts_local_path = builder.code_path / "dist"
    artifacts_remote_path = docs_remote_path / "download"
    if artifacts_local_path.exists():
        artifacts_source_path = f"{artifacts_local_path}/"
        artifact_dest_path = f"{connect_id}:{artifacts_remote_path}/"
        worker.utils.rsync(
            artifacts_source_path,
            artifact_dest_path,
            change_modes=change_modes,
            change_owner=change_owner,
            port=worker_config.studio_port,
            dry_run=dry_run,
        )
    else:
        worker.utils.info("No downloadable artifacts to be copied over")


if __name__ == "__main__":
    steps: worker.utils.BuilderSteps = OrderedDict()
    steps["update"] = update
    steps["compile"] = compile_doc
    steps["deliver"] = deliver

    parser = worker.utils.create_argument_parser(steps=steps)
    parser.add_argument("--needs-package-delivery", action="store_true", required=False)

    args = parser.parse_args()
    builder = DocStudioBuilder(args)
    builder.run(args.step, steps)
