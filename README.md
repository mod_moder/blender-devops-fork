# Blender DevOps Fork

Scripts to manage Blender development infrastructure.

Currently contains the buildbot worker and master code, used for:
* Daily builds
* Patch builds
* Release packaging, signing and deployment
* Documentation building and deployment

Issues are tracked in [infrastructure/blender-projects-platform](https://projects.blender.org/infrastructure/blender-projects-platform/issues).