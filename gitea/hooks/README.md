# Git Hooks

Server side Git hooks to prevent invalid pushes.

Installed in Gitea as follows:
```
cp blender_merge_hook /path/to/data/gitea-repositories/blender/blender.git/hooks/update.d/blender_merge_hook
cp blender_merge_hook /path/to/data/gitea-repositories/blender/blender-addons-contrib.git/hooks/update.d/blender_merge_hook
cp blender_merge_hook /path/to/data/gitea-repositories/blender/blender-addons.git/hooks/update.d/blender_merge_hook
cp blender_merge_hook /path/to/data/gitea-repositories/blender/blender-assets.git/hooks/update.d/blender_merge_hook
cp blender_merge_hook /path/to/data/gitea-repositories/blender/blender-manual.git/hooks/update.d/blender_merge_hook
cp blender_merge_hook /path/to/data/gitea-repositories/blender/blender-test-data.git/hooks/update.d/blender_merge_hook
cp blender_merge_hook /path/to/data/gitea-repositories/blender/lib-linux_x64.git/hooks/update.d/blender_merge_hook
cp blender_merge_hook /path/to/data/gitea-repositories/blender/lib-macos_arm64.git/hooks/update.d/blender_merge_hook
cp blender_merge_hook /path/to/data/gitea-repositories/blender/lib-macos_x64.git/hooks/update.d/blender_merge_hook
cp blender_merge_hook /path/to/data/gitea-repositories/blender/lib-source.git/hooks/update.d/blender_merge_hook
cp blender_merge_hook /path/to/data/gitea-repositories/blender/lib-windows_arm64.git/hooks/update.d/blender_merge_hook
cp blender_merge_hook /path/to/data/gitea-repositories/blender/lib-windows_x64.git/hooks/update.d/blender_merge_hook
```
